"""
Source Code for Homework 4.b of ECBM E4040, Fall 2016, Columbia University

"""

import os
import timeit
import inspect
import sys
import numpy
from collections import OrderedDict
import random
import theano
import theano.tensor as T
from theano.tensor.nnet import conv2d
import numpy as np

from hw4_utils import contextwin, shared_dataset, load_data, shuffle, conlleval, check_dir
from hw4_nn import myMLP, train_nn,LogisticRegression

def gen_parity_pair(nbit, num):
    """
    Generate binary sequences and their parity bits

    :type nbit: int
    :param nbit: length of binary sequence

    :type num: int
    :param num: number of sequences

    """
    X = numpy.random.randint(2, size=(num, nbit))
    Y = numpy.mod(numpy.sum(X, axis=1), 2)
    
    return X, Y
    
    
def gen_parity_pair_rnn(nbit, num):
    """
    Generate binary sequences and their parity bits

    :type nbit: int
    :param nbit: length of binary sequence

    :type num: int
    :param num: number of sequences

    """
    X = numpy.random.randint(2, size=(num, nbit))
    Y = numpy.array(X)
    for i in range(1,nbit):
        Y[:,i] += Y[:, i-1]

    Y = numpy.mod(Y, 2)
    return X, Y
    
def sample_weights(sizeX, sizeY):
        values = np.ndarray([sizeX, sizeY], dtype=dtype)
        for dx in xrange(sizeX):
            vals = np.random.uniform(low=-0.2, high=0.2,  size=(sizeY,))
            #vals_norm = np.sqrt((vals**2).sum())
            #vals = vals / vals_norm
            values[dx,:] = vals
        _,svs,_ = np.linalg.svd(values)
        #svs[0] is the largest singular value                      
        values = values / svs[0]
        return values  
        
sigma = lambda x: 1 / (1 + T.exp(-x))
dtype=theano.config.floatX  
#TODO: implement RNN class to learn parity function
class RNN(object):
    """ Elman Neural Net Model Class
    """
    def __init__(self, input_x, nh):
        """Initialize the parameters for the RNN

        :type nh: int
        :param nh: dimension of the hidden layer
        """
        # parameters of the model
        self.wx = theano.shared(name='wx',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (1, nh))
                                .astype(theano.config.floatX))
        self.wh = theano.shared(name='wh',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (nh, nh))
                                .astype(theano.config.floatX))
        self.bh = theano.shared(name='bh',
                                value=numpy.zeros(nh,
                                dtype=theano.config.floatX))
        self.h0 = theano.shared(name='h0',
                                value=numpy.zeros(nh,
                                dtype=theano.config.floatX))

        def recurrence(x_t, h_tm1):
            h_t = T.nnet.sigmoid(T.dot(x_t, self.wx)
                                 + T.dot(h_tm1, self.wh) + self.bh)
            return h_t

        h, _ = theano.scan(fn=recurrence,
                              sequences=input_x,
                              outputs_info=[self.h0],
                              n_steps=input_x.shape[0])

        self.logRegressionLayer = LogisticRegression(
            input=h,
            n_in=nh,
            n_out=2
        )

        self.negative_log_likelihood = (
            self.logRegressionLayer.negative_log_likelihood
        )

        self.errors = self.logRegressionLayer.errors

        self.params = [self.wx, self.wh, self.bh] + self.logRegressionLayer.params

        self.input = input_x
    

#TODO: implement LSTM class to learn parity function
class LSTM(object):        
    
      
          
    def __init__(self,input_x, n_in, n_i,n_hidden,n_c,n_f, n_o,n_y):
        self.W_xi = theano.shared(sample_weights(n_in, n_i))  
        self.W_hi = theano.shared(sample_weights(n_hidden, n_i))  
        #self.W_ci = theano.shared(sample_weights(n_c, n_i))  
        self.b_i = theano.shared(np.cast[dtype](np.random.uniform(0,0,size = n_i)))
        
        self.W_xf = theano.shared(sample_weights(n_in, n_f)) 
        self.W_hf = theano.shared(sample_weights(n_hidden, n_f))
        #self.W_cf = theano.shared(sample_weights(n_c, n_f))
        self.b_f = theano.shared(np.cast[dtype](np.random.uniform(0, 0,size = n_f)))
        
        self.W_xc = theano.shared(sample_weights(n_in, n_c))  
        self.W_hc = theano.shared(sample_weights(n_hidden, n_c))
        self.b_c = theano.shared(np.zeros(n_c, dtype=dtype))
        
        self.W_xo = theano.shared(sample_weights(n_in, n_o))
        self.W_ho = theano.shared(sample_weights(n_hidden, n_o))
        self.W_co = theano.shared(sample_weights(n_c, n_o))
        self.b_o = theano.shared(np.cast[dtype](np.random.uniform(0,0,size = n_o)))
        
        self.W_hy = theano.shared(sample_weights(n_hidden, n_y))
        self.b_y = theano.shared(np.zeros(n_y, dtype=dtype))

        self.c0 = theano.shared(np.zeros(n_hidden, dtype=dtype))
        self.h0 = T.tanh(self.c0)

        self.params = [self.W_xi, self.W_hi,  self.b_i,self.W_xf, self.W_hf,
                        self.b_f, self.W_xc, self.W_hc, self.b_c,self.W_xo, self.W_ho, self.W_co]  
    
        def recurrence(x_t, h_tm1, c_tm1#, W_xi, W_hi, W_ci, b_i, W_xf, W_hf, W_cf, b_f, W_xc, W_hc, b_c, W_xy, W_ho, W_cy, b_o, W_hy, b_y
             ):
            i_t = sigma(theano.dot(x_t, self.W_xi) + theano.dot(h_tm1, self.W_hi) + self.b_i)
            f_t = sigma(theano.dot(x_t, self.W_xf) + theano.dot(h_tm1, self.W_hf) + self.b_f)
            c_t = f_t * c_tm1 + i_t * T.tanh(theano.dot(x_t, self.W_xc) + theano.dot(h_tm1, self.W_hc) + self.b_c) 
            o_t = sigma(theano.dot(x_t, self.W_xo)+ theano.dot(h_tm1, self.W_ho) + theano.dot(c_t, self.W_co)  + self.b_o)
            h_t = o_t * T.tanh(c_t)
            y_t = sigma(theano.dot(h_t, self.W_hy) + self.b_y) 
            return [h_t, c_t, y_t]
        
    
        v = T.matrix(dtype=dtype)
 
        # target
        #target = T.matrix(dtype=dtype)
        [h_vals, _, y_vals], _ = theano.scan(fn=recurrence, 
                                  sequences = input_x, 
                                  outputs_info = [self.h0, self.c0, None ], # corresponds to return type of fn
                                  #non_sequences = [W_xi, W_hi, W_ci, b_i, W_xf, W_hf, W_cf, b_f, W_xc, W_hc, b_c, W_xo, W_ho, W_co, b_o, W_hy, b_y]
                                  )

        self.logRegressionLayer = LogisticRegression(
            input=h_vals,
            n_in=n_hidden,
            n_out=2
        )

        self.negative_log_likelihood = (
            self.logRegressionLayer.negative_log_likelihood
        )

        self.errors = self.logRegressionLayer.errors

        self.params = self.params + self.logRegressionLayer.params

        self.input = input_x

    
#TODO: build and train a MLP to learn parity function
def test_mlp_parity(learning_rate=0.01, L1_reg=0.0001, L2_reg=0.0001, n_epochs=100,
             batch_size=128, n_hidden=50, n_hiddenLayers=1,
             verbose=False,input_bit=8):
    # generate datasets
    train_set = gen_parity_pair(input_bit, 1000)
    valid_set = gen_parity_pair(input_bit, 500)
    test_set  = gen_parity_pair(input_bit, 100)

    # Convert raw dataset to Theano shared variables.
    train_set_x, train_set_y = shared_dataset(train_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    test_set_x, test_set_y = shared_dataset(test_set)
    
    # compute number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch
    x = T.matrix('x')  # the data is presented as rasterized images
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels

    rng = numpy.random.RandomState(1234)

    # TODO: construct a neural network, either MLP or CNN.
    classifier = myMLP(rng=rng,
                       input=x,
                       n_in=input_bit,
                       n_hidden=n_hidden,
                       n_out=1,
                       n_hiddenLayers=n_hiddenLayers
                      )

    # the cost we minimize during training is the negative log likelihood of
    # the model plus the regularization terms (L1 and L2); cost is expressed
    # here symbolically
    cost = (
        classifier.negative_log_likelihood(y)
        + L1_reg * classifier.L1
        + L2_reg * classifier.L2_sqr
    )

    # compiling a Theano function that computes the mistakes that are made
    # by the model on a minibatch
    test_model = theano.function(
        inputs=[index],
        outputs=classifier.errors(y),
        givens={
            x: test_set_x[index * batch_size:(index + 1) * batch_size],
            y: test_set_y[index * batch_size:(index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        inputs=[index],
        outputs=classifier.errors(y),
        givens={
            x: valid_set_x[index * batch_size:(index + 1) * batch_size],
            y: valid_set_y[index * batch_size:(index + 1) * batch_size]
        }
    )

    # compute the gradient of cost with respect to theta (sotred in params)
    # the resulting gradients will be stored in a list gparams
    gparams = [T.grad(cost, param) for param in classifier.params]

    # specify how to update the parameters of the model as a list of
    # (variable, update expression) pairs

    # given two lists of the same length, A = [a1, a2, a3, a4] and
    # B = [b1, b2, b3, b4], zip generates a list C of same size, where each
    # element is a pair formed from the two lists :
    #    C = [(a1, b1), (a2, b2), (a3, b3), (a4, b4)]
    updates = [
        (param, param - learning_rate * gparam)
        for param, gparam in zip(classifier.params, gparams)
    ]

    # compiling a Theano function `train_model` that returns the cost, but
    # in the same time updates the parameter of the model based on the rules
    # defined in `updates`
    train_model = theano.function(
        inputs=[index],
        outputs=cost,
        updates=updates,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size],
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    ###############
    # TRAIN MODEL #
    ###############
    print('... training')

    return train_nn(train_model, validate_model, test_model,
        n_train_batches, n_valid_batches, n_test_batches, n_epochs, verbose)
    

    
#TODO: build and train a RNN to learn parity function
def test_rnn_parity(**kwargs):
    param = {
        'lr': 0.05,
        'verbose': True,
        'nhidden': 12,
        'nbit': 8,
        'seed': 345,
        'nepochs': 10000}
    param_diff = set(kwargs.keys()) - set(param.keys())
    if param_diff:
        raise KeyError("invalid arguments:" + str(tuple(param_diff)))
    param.update(kwargs)

    numpy.random.seed(param['seed'])
    random.seed(param['seed'])

    # Generate datasets
    train_set = gen_parity_pair_rnn(param['nbit'], 10000)
    valid_set = gen_parity_pair_rnn(param['nbit'], 5000)
    test_set  = gen_parity_pair_rnn(param['nbit'], 1000)

    # Convert raw dataset to Theano shared variables.
    train_set_x, train_set_y = shared_dataset(train_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    test_set_x, test_set_y = shared_dataset(test_set)

    # Compute number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0]
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
    n_test_batches = test_set_x.get_value(borrow=True).shape[0]

    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch
    x = T.matrix('x')  # the data is presented as a matrix
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels

    rnn = RNN(
        input_x=x,
        nh=param['nhidden'])

    # train with early stopping on validation set
    print('... training')

    cost = (
        rnn.negative_log_likelihood(y)
    )

    # compiling a Theano function that computes the mistakes that are made
    # by the model on a minibatch
    test_model = theano.function(
        inputs=[index],
        outputs=rnn.errors(y),
        givens={
            # Shuffle dim as dot product has been changed
            x: test_set_x[index * 1:(index + 1) * 1].dimshuffle(1,0),
            y: test_set_y[index * 1:(index + 1) * 1][0]
        }
    )

    validate_model = theano.function(
        inputs=[index],
        outputs=rnn.errors(y),
        givens={
            x: valid_set_x[index * 1:(index + 1) * 1].dimshuffle(1,0),

            y: valid_set_y[index * 1:(index + 1) * 1][0]

        }
    )

    # compute the gradient of cost with respect to theta (sotred in params)
    # the resulting gradients will be stored in a list gparams
    gparams = [T.grad(cost, p) for p in rnn.params]

    # specify how to update the parameters of the model as a list of
    # (variable, update expression) pairs

    # given two lists of the same length, A = [a1, a2, a3, a4] and
    # B = [b1, b2, b3, b4], zip generates a list C of same size, where each
    # element is a pair formed from the two lists :
    #    C = [(a1, b1), (a2, b2), (a3, b3), (a4, b4)]
    updates = [
        (p, p - param['lr'] * gparam)
        for p, gparam in zip(rnn.params, gparams)
    ]

    # compiling a Theano function `train_model` that returns the cost, but
    # in the same time updates the parameter of the model based on the rules
    # defined in `updates`
    train_model = theano.function(
        inputs=[index],
        outputs=cost,
        updates=updates,
        givens={
            x: train_set_x[index * 1: (index + 1) * 1].dimshuffle(1,0),
            y: train_set_y[index * 1: (index + 1) * 1][0]
        }
    )

    ###############
    # TRAIN MODEL #
    ###############
    print('... training')

    train_nn(train_model, validate_model, test_model,
        n_train_batches, n_valid_batches, n_test_batches, param['nepochs'], param['verbose'])


    
#TODO: build and train a LSTM to learn parity function
def test_lstm_parity(**kwargs):
    
    param = {
        'lr': 0.05,
        'verbose': True,
        'n_in':1,
        'n_i':1,
        'n_hidden':1,
        'n_c':1,
        'n_f':1, 
        'n_o':1,
        'n_y':1,
        'nbit': 8,
        'seed': 345,
        'nepochs': 10000}
    param_diff = set(kwargs.keys()) - set(param.keys())
    if param_diff:
        raise KeyError("invalid arguments:" + str(tuple(param_diff)))
    param.update(kwargs)

    numpy.random.seed(param['seed'])
    random.seed(param['seed'])

    # Generate datasets
    train_set = gen_parity_pair_rnn(param['nbit'], 10000)
    valid_set = gen_parity_pair_rnn(param['nbit'], 5000)
    test_set  = gen_parity_pair_rnn(param['nbit'], 1000)

    # Convert raw dataset to Theano shared variables.
    train_set_x, train_set_y = shared_dataset(train_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    test_set_x, test_set_y = shared_dataset(test_set)

    # Compute number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0]
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
    n_test_batches = test_set_x.get_value(borrow=True).shape[0]

    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch
    x = T.matrix('x')  # the data is presented as a matrix
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels

    lstm = LSTM(
        input_x=x,
        n_in=param['n_in'],
        n_i=param['n_i'],
        n_hidden=param['n_hidden'],
        n_c=param['n_c'],
        n_f=param['n_f'], 
        n_o=param['n_o'],
        n_y=param['n_y'],
        )

    # train with early stopping on validation set
    print('... training')

    cost = (
        lstm.negative_log_likelihood(y)
    )

    # compiling a Theano function that computes the mistakes that are made
    # by the model on a minibatch
    test_model = theano.function(
        inputs=[index],
        outputs=lstm.errors(y),
        givens={
            # Shuffle dim as dot product has been changed
            x: test_set_x[index * 1:(index + 1) * 1].dimshuffle(1,0),
            y: test_set_y[index * 1:(index + 1) * 1][0]
        }
    )

    validate_model = theano.function(
        inputs=[index],
        outputs=lstm.errors(y),
        givens={
            x: valid_set_x[index * 1:(index + 1) * 1].dimshuffle(1,0),

            y: valid_set_y[index * 1:(index + 1) * 1][0]

        }
    )

    # compute the gradient of cost with respect to theta (sotred in params)
    # the resulting gradients will be stored in a list gparams
    gparams = [T.grad(cost, p) for p in lstm.params]

    # specify how to update the parameters of the model as a list of
    # (variable, update expression) pairs

    # given two lists of the same length, A = [a1, a2, a3, a4] and
    # B = [b1, b2, b3, b4], zip generates a list C of same size, where each
    # element is a pair formed from the two lists :
    #    C = [(a1, b1), (a2, b2), (a3, b3), (a4, b4)]
    updates = [
        (p, p - param['lr'] * gparam)
        for p, gparam in zip(lstm.params, gparams)
    ]

    # compiling a Theano function `train_model` that returns the cost, but
    # in the same time updates the parameter of the model based on the rules
    # defined in `updates`
    train_model = theano.function(
        inputs=[index],
        outputs=cost,
        updates=updates,
        givens={
            x: train_set_x[index * 1: (index + 1) * 1].dimshuffle(1,0),
            y: train_set_y[index * 1: (index + 1) * 1][0]
        }
    )

    ###############
    # TRAIN MODEL #
    ###############
    print('... training')

    train_nn(train_model, validate_model, test_model,
        n_train_batches, n_valid_batches, n_test_batches, param['nepochs'], param['verbose'])


    
if __name__ == '__main__':
    #test_mlp_parity(n_hidden=2)
    #test_rnn_parity()
    test_lstm_parity()