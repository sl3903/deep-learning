import os
from os import walk
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy.linalg as linalg

from PIL import Image

import theano
import theano.tensor as T
from theano.tensor.nnet.neighbours import images2neibs


'''
Implement the functions that were not implemented and complete the
parts of main according to the instructions in comments.
'''

def reconstructed_image(D,c,num_coeffs,X_mean,n_blocks,im_num):
    '''
    This function reconstructs an image X_recon_img given the number of
    coefficients for each image specified by num_coeffs
    '''
    
    '''
        Parameters
    ---------------
    c: np.ndarray
        a n x m matrix  representing the coefficients of all the image blocks.
        n represents the maximum dimension of the PCA space.
        m is (number of images x n_blocks**2)

    D: np.ndarray
        an N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in a block)

    im_num: Integer
        index of the image to visualize

    X_mean: np.ndarray
        a matrix representing the mean block.

    num_coeffs: Integer
        an integer that specifies the number of top components to be
        considered while reconstructing
        

    n_blocks: Integer
        number of blocks comprising the image in each direction.
        For example, for a 256x256 image divided into 64x64 blocks, n_blocks will be 4
    '''
    
    c_im = c[:num_coeffs,n_blocks*n_blocks*im_num:n_blocks*n_blocks*(im_num+1)]
    D_im = D[:,:num_coeffs]
    
    #TODO: Enter code below for reconstructing the image X_recon_img
    X_recon_img =None
    for i in range(n_blocks):
        Y = None
        for j in range(n_blocks):
            Y = np.append(Y, np.dot(D_im, c_im[:, i*n_blocks+j]).reshape(X_mean.shape) + X_mean, 1) \
                if Y is not None else np.dot(D_im, c_im[:,i*n_blocks+j]).reshape(X_mean.shape) + X_mean    
        X_recon_img= np.append(X_recon_img, Y, 0) if X_recon_img is not None else Y
  
  
  
  
  
    #X_recon_img = ........
    return X_recon_img

def plot_reconstructions(D,c,num_coeff_array,X_mean,n_blocks,im_num):
    '''
    Plots 9 reconstructions of a particular image using D as the basis matrix and coeffiecient
    vectors from c

    Parameters
    ------------------------
        num_coeff_array: Iterable
            an iterable with 9 elements representing the number of coefficients
            to use for reconstruction for each of the 9 plots
        
        c: np.ndarray
            a l x m matrix  representing the coefficients of all blocks in a particular image
            l represents the dimension of the PCA space used for reconstruction
            m represents the number of blocks in an image

        D: np.ndarray
            an N x l matrix representing l basis vectors of the PCA space
            N is the dimension of the original space (number of pixels in a block)

        n_blocks: Integer
            number of blocks comprising the image in each direction.
            For example, for a 256x256 image divided into 64x64 blocks, n_blocks will be 4

        X_mean: basis vectors represent the divergence from the mean so this
            matrix should be added to all reconstructed blocks

        im_num: Integer
            index of the image to visualize
    '''
    print(c.shape, D.shape, n_blocks)
    f, axarr = plt.subplots(3,3)
    for i in range(3):
        for j in range(3):
            plt.axes(axarr[i,j])
            plt.imshow(reconstructed_image(D,c,num_coeff_array[i*3+j],X_mean,n_blocks,im_num))
            
    f.savefig('/Users/luansitao/e4040_hw1_sl3903/output/hw1a_{0}_im{1}.png'.format(n_blocks, im_num))
    plt.close(f)
    
    
    
def plot_top_16(D, sz, imname):
    '''
    Plots the top 16 components from the basis matrix D.
    Each basis vector represents an image block of shape (sz, sz)

    Parameters
    -------------
    D: np.ndarray
        N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in a block)
        n represents the maximum dimension of the PCA space (assumed to be atleast 16)

    sz: Integer
        The height and width of each block

    imname: string
        name of file where image will be saved.
    '''
    #TODO: Obtain top 16 components of D and plot them
    f, axarr = plt.subplots(4, 4)
    for i in range(axarr.shape[0]):
        for j in range(axarr.shape[1]):
            axarr[i, j].imshow(D[:, i*4+j].reshape((sz, sz)), cmap=cm.Greys_r)
            axarr[i, j].set_title(i*4+j+1)
            axarr[i, j].axis('off')
    f.savefig(imname, bbox_inches='tight')
    plt.close(f)


    


    
def main():
    '''
    Read here all images(grayscale) from Fei_256 folder
    into an numpy array Ims with size (no_images, height, width).
    Make sure the images are read after sorting the filenames
    '''
    #TODO: Read all images into a numpy array of size (no_images, height, width)
    
    fnames = []
    for (_, _, f) in walk('/Users/luansitao/e4040_hw1_sl3903/Fei_256'):
        fnames.extend(f)

    fnames.sort()
    I = []
    for filename in fnames:
        try:
            I.append(np.asarray(Image.open("/Users/luansitao/e4040_hw1_sl3903/Fei_256/{0}".format(filename)).convert('L')))
        except:
            continue
        
    szs = [8, 32, 64]
    num_coeffs = [range(1, 10, 1), range(3, 30, 3), range(5, 50, 5)]
    Ims = np.asarray(I, np.float32)
    
    
    for sz, nc in zip(szs, num_coeffs):
        '''
        Divide here each image into non-overlapping blocks of shape (sz, sz).
        Flatten each block and arrange all the blocks in a
        (no_images*n_blocks_in_image) x (sz*sz) matrix called X
        ''' 
        
        #TODO: Write a code snippet that performs as indicated in the above comment
        images = T.tensor4('images')
        neibs = images2neibs(images, neib_shape=(sz, sz))
        window_function = theano.function([images], neibs)
        X = window_function(Ims.reshape([Ims.shape[0], 1, Ims.shape[1], Ims.shape[2]]))


        X_mean = np.mean(X, 0)
        X = X - np.repeat(X_mean.reshape(1, -1), X.shape[0], 0)

        '''
        Perform eigendecomposition on X^T X and arrange the eigenvectors
        in decreasing order of eigenvalues into a matrix D
        '''
        
        #TODO: Write a code snippet that performs as indicated in the above comment
        w, v = np.linalg.eigh(np.dot(X.T, X))

        v = v[:, np.argsort(w)]
        D = np.fliplr(v)
        
        
        c = np.dot(D.T, X.T)
        
        for i in range(0, 200, 10):
            plot_reconstructions(D=D, c=c, num_coeff_array=nc, X_mean=X_mean.reshape((sz, sz)), n_blocks=int(256/sz), im_num=i)

        plot_top_16(D, sz, imname='/Users/luansitao/e4040_hw1_sl3903/output/hw1a_top16_{0}.png'.format(sz))


if __name__ == '__main__':
    main()
    
    