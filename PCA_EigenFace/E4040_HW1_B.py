import os
from os import walk
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy.linalg as linalg

from PIL import Image

import theano
import theano.tensor as T
from theano.tensor.nnet.neighbours import images2neibs



'''
Implement the functions that were not implemented and complete the
parts of main according to the instructions in comments.
'''

def reconstructed_image(D,c,num_coeffs,X_mean,im_num):
    '''
    This function reconstructs an image given the number of
    coefficients for each image specified by num_coeffs
    '''
    
    '''
        Parameters
    ---------------
    c: np.ndarray
        a n x m matrix  representing the coefficients of all the image blocks.
        n represents the maximum dimension of the PCA space.
        m is (number of images x n_blocks**2)

    D: np.ndarray
        an N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in a block)

    im_num: Integer
        index of the image to visualize

    X_mean: np.ndarray
        a matrix representing the mean block.

    num_coeffs: Integer
        an integer that specifies the number of top components to be
        considered while reconstructing
    '''
    
    c_im = c[:num_coeffs,im_num]
    D_im = D[:,:num_coeffs]
    
    #TODO: Enter code below for reconstructing the image
    X_recon_img = np.dot(D_im, c_im).reshape(X_mean.shape) + X_mean
   
    #X_recon_img = ........
    return X_recon_img

def plot_reconstructions(D,c,num_coeff_array,X_mean,im_num):
    '''
    Plots 9 reconstructions of a particular image using D as the basis matrix and coeffiecient
    vectors from c

    Parameters
    ------------------------
        num_coeff_array: Iterable
            an iterable with 9 elements representing the number_of coefficients
            to use for reconstruction for each of the 9 plots
        
        c: np.ndarray
            a l x m matrix  representing the coefficients of all blocks in a particular image
            l represents the dimension of the PCA space used for reconstruction
            m represents the number of blocks in an image

        D: np.ndarray
            an N x l matrix representing l basis vectors of the PCA space
            N is the dimension of the original space (number of pixels in a block)

        X_mean: basis vectors represent the divergence from the mean so this
            matrix should be added to all reconstructed blocks

        im_num: Integer
            index of the image to visualize
    '''
    f, axarr = plt.subplots(3,3)
    for i in range(3):
        for j in range(3):
            plt.axes(axarr[i,j])
            plt.imshow(reconstructed_image(D,c,num_coeff_array[i*3+j],X_mean,im_num))
            
    f.savefig('/Users/luansitao/e4040_hw1_sl3903/output2/hw1b_{0}.png'.format(im_num),cmap=cm.Greys_r)
    plt.close(f)
    
    
    
def plot_top_16(D, sz, imname):
    '''
    Plots the top 16 components from the basis matrix D.
    Each basis vector represents an image block of shape (sz, sz)

    Parameters
    -------------
    D: np.ndarray
        N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in a block)
        n represents the maximum dimension of the PCA space (assumed to be atleast 16)

    sz: Integer
        The height and width of each block

    imname: string
        name of file where image will be saved.
    '''
    #TODO: Obtain top 16 components of D and plot them
    
    f, axarr = plt.subplots(4, 4)
    for i in range(axarr.shape[0]):
        for j in range(axarr.shape[1]):
            axarr[i, j].imshow(D[:, i*4+j].reshape((sz, sz)), cmap=cm.Greys_r)
            axarr[i, j].axis('off')

    f.savefig(imname, bbox_inches='tight')
    plt.close(f)

    
def main():
    '''
    Read here all images(grayscale) from Fei_256 folder and collapse 
    each image to get an numpy array Ims with size (no_images, height*width).
    Make sure the images are read after sorting the filenames
    '''
    #TODO: Write a code snippet that performs as indicated in the above comment
    fnames = []
    for (_, _, f) in walk('/Users/luansitao/e4040_hw1_sl3903/Fei_256'):
        fnames.extend(f)

    fnames.sort()
    I = None
    for filename in fnames:
        try:
            I = np.append(I, np.asarray(Image.open("/Users/luansitao/e4040_hw1_sl3903/Fei_256/{0}".format(filename)).convert('L')).reshape((1, -1)), axis=0) \
                if I is not None else np.asarray(Image.open("/Users/luansitao/e4040_hw1_sl3903/Fei_256/{0}".format(filename)).convert('L')).reshape((1, -1))
        except:
            continue

    Ims = I.astype(np.float32)
    X_mean = np.mean(Ims, 0)
    X = Ims - np.repeat(X_mean.reshape(1, -1), Ims.shape[0], 0)


    '''
    Use theano to perform gradient descent to get top 16 PCA components of X
    Put them into a matrix D with decreasing order of eigenvalues

    If you are not using the provided AMI and get an error "Cannot construct a ufunc with more than 32 operands" :
    You need to perform a patch to theano from this pull(https://github.com/Theano/Theano/pull/3532)
    Alternatively you can downgrade numpy to 1.9.3, scipy to 0.15.1, matplotlib to 1.4.2
    '''
    
    #TODO: Write a code snippet that performs as indicated in the above comment
    n_eig = 16

    w = []
    v = []
    vs = []
    XX = T.dmatrix('XX')
    learning_rate = 0.1

    for i in range(n_eig):
        v.append(theano.shared(np.random.randn(X.shape[1]), name='v{0}'.format(i)))
        cost = T.dot(v[-1].T, T.dot(XX.T, T.dot(XX, v[-1])))
        for j in range(i):
            cost = cost - (w[j]*(T.dot(v[-1].T, v[j]))**2)
        cost = -cost
        gc_v = T.grad(cost, v[-1])

        dsc = theano.function([XX], cost, updates = [(v[-1], v[-1]-learning_rate*gc_v)])
        normalize = theano.function([], [], updates=[(v[-1], v[-1] / T.sqrt(T.dot(v[-1].T, v[-1])))])

        normalize()
        max_steps = 200
        cst = [np.inf]
        cst.append(dsc(X).item(0))
        normalize()

        j = 0
        while j<max_steps and cst[-2]-cst[-1]>1e-2:
            cst.append(dsc(X).item(0))
            normalize()
            if j%10 == 0:
                print i, j, cst[-1]
            j+=1

        vs.append(v[-1].get_value())
        w.append(np.dot(vs[-1].T, np.dot(X.T, np.dot(X, vs[-1]))))

    D = None
    for i in range(n_eig):
        D = np.append(D, vs[i].reshape(-1, 1), axis=1) if D is not None else vs[i].reshape(-1, 1)

    c = np.dot(D.T, X.T)

   
  
       
       
       
       
       
    for i in range(0, 200, 10):
        plot_reconstructions(D=D, c=c, num_coeff_array=[1, 2, 4, 6, 8, 10, 12, 14, 16], X_mean=X_mean.reshape((256, 256)), im_num=i)

    plot_top_16(D, 256, '/Users/luansitao/e4040_hw1_sl3903/output2/hw1b_top16_256.png')


if __name__ == '__main__':
    main()
    
    